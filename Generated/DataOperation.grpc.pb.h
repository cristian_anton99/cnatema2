// Generated by the gRPC C++ plugin.
// If you make any local change, they will be lost.
// source: DataOperation.proto
#ifndef GRPC_DataOperation_2eproto__INCLUDED
#define GRPC_DataOperation_2eproto__INCLUDED

#include "DataOperation.pb.h"

#include <functional>
#include <grpc/impl/codegen/port_platform.h>
#include <grpcpp/impl/codegen/async_generic_service.h>
#include <grpcpp/impl/codegen/async_stream.h>
#include <grpcpp/impl/codegen/async_unary_call.h>
#include <grpcpp/impl/codegen/client_callback.h>
#include <grpcpp/impl/codegen/client_context.h>
#include <grpcpp/impl/codegen/completion_queue.h>
#include <grpcpp/impl/codegen/message_allocator.h>
#include <grpcpp/impl/codegen/method_handler.h>
#include <grpcpp/impl/codegen/proto_utils.h>
#include <grpcpp/impl/codegen/rpc_method.h>
#include <grpcpp/impl/codegen/server_callback.h>
#include <grpcpp/impl/codegen/server_callback_handlers.h>
#include <grpcpp/impl/codegen/server_context.h>
#include <grpcpp/impl/codegen/service_type.h>
#include <grpcpp/impl/codegen/status.h>
#include <grpcpp/impl/codegen/stub_options.h>
#include <grpcpp/impl/codegen/sync_stream.h>

class DataOperationService final {
 public:
  static constexpr char const* service_full_name() {
    return "DataOperationService";
  }
  class StubInterface {
   public:
    virtual ~StubInterface() {}
    virtual ::grpc::Status SaySign(::grpc::ClientContext* context, const ::DataRequest& request, ::DataResponse* response) = 0;
    std::unique_ptr< ::grpc::ClientAsyncResponseReaderInterface< ::DataResponse>> AsyncSaySign(::grpc::ClientContext* context, const ::DataRequest& request, ::grpc::CompletionQueue* cq) {
      return std::unique_ptr< ::grpc::ClientAsyncResponseReaderInterface< ::DataResponse>>(AsyncSaySignRaw(context, request, cq));
    }
    std::unique_ptr< ::grpc::ClientAsyncResponseReaderInterface< ::DataResponse>> PrepareAsyncSaySign(::grpc::ClientContext* context, const ::DataRequest& request, ::grpc::CompletionQueue* cq) {
      return std::unique_ptr< ::grpc::ClientAsyncResponseReaderInterface< ::DataResponse>>(PrepareAsyncSaySignRaw(context, request, cq));
    }
    class experimental_async_interface {
     public:
      virtual ~experimental_async_interface() {}
      virtual void SaySign(::grpc::ClientContext* context, const ::DataRequest* request, ::DataResponse* response, std::function<void(::grpc::Status)>) = 0;
      virtual void SaySign(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::DataResponse* response, std::function<void(::grpc::Status)>) = 0;
      #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
      virtual void SaySign(::grpc::ClientContext* context, const ::DataRequest* request, ::DataResponse* response, ::grpc::ClientUnaryReactor* reactor) = 0;
      #else
      virtual void SaySign(::grpc::ClientContext* context, const ::DataRequest* request, ::DataResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) = 0;
      #endif
      #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
      virtual void SaySign(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::DataResponse* response, ::grpc::ClientUnaryReactor* reactor) = 0;
      #else
      virtual void SaySign(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::DataResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) = 0;
      #endif
    };
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
    typedef class experimental_async_interface async_interface;
    #endif
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
    async_interface* async() { return experimental_async(); }
    #endif
    virtual class experimental_async_interface* experimental_async() { return nullptr; }
  private:
    virtual ::grpc::ClientAsyncResponseReaderInterface< ::DataResponse>* AsyncSaySignRaw(::grpc::ClientContext* context, const ::DataRequest& request, ::grpc::CompletionQueue* cq) = 0;
    virtual ::grpc::ClientAsyncResponseReaderInterface< ::DataResponse>* PrepareAsyncSaySignRaw(::grpc::ClientContext* context, const ::DataRequest& request, ::grpc::CompletionQueue* cq) = 0;
  };
  class Stub final : public StubInterface {
   public:
    Stub(const std::shared_ptr< ::grpc::ChannelInterface>& channel);
    ::grpc::Status SaySign(::grpc::ClientContext* context, const ::DataRequest& request, ::DataResponse* response) override;
    std::unique_ptr< ::grpc::ClientAsyncResponseReader< ::DataResponse>> AsyncSaySign(::grpc::ClientContext* context, const ::DataRequest& request, ::grpc::CompletionQueue* cq) {
      return std::unique_ptr< ::grpc::ClientAsyncResponseReader< ::DataResponse>>(AsyncSaySignRaw(context, request, cq));
    }
    std::unique_ptr< ::grpc::ClientAsyncResponseReader< ::DataResponse>> PrepareAsyncSaySign(::grpc::ClientContext* context, const ::DataRequest& request, ::grpc::CompletionQueue* cq) {
      return std::unique_ptr< ::grpc::ClientAsyncResponseReader< ::DataResponse>>(PrepareAsyncSaySignRaw(context, request, cq));
    }
    class experimental_async final :
      public StubInterface::experimental_async_interface {
     public:
      void SaySign(::grpc::ClientContext* context, const ::DataRequest* request, ::DataResponse* response, std::function<void(::grpc::Status)>) override;
      void SaySign(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::DataResponse* response, std::function<void(::grpc::Status)>) override;
      #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
      void SaySign(::grpc::ClientContext* context, const ::DataRequest* request, ::DataResponse* response, ::grpc::ClientUnaryReactor* reactor) override;
      #else
      void SaySign(::grpc::ClientContext* context, const ::DataRequest* request, ::DataResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) override;
      #endif
      #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
      void SaySign(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::DataResponse* response, ::grpc::ClientUnaryReactor* reactor) override;
      #else
      void SaySign(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::DataResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) override;
      #endif
     private:
      friend class Stub;
      explicit experimental_async(Stub* stub): stub_(stub) { }
      Stub* stub() { return stub_; }
      Stub* stub_;
    };
    class experimental_async_interface* experimental_async() override { return &async_stub_; }

   private:
    std::shared_ptr< ::grpc::ChannelInterface> channel_;
    class experimental_async async_stub_{this};
    ::grpc::ClientAsyncResponseReader< ::DataResponse>* AsyncSaySignRaw(::grpc::ClientContext* context, const ::DataRequest& request, ::grpc::CompletionQueue* cq) override;
    ::grpc::ClientAsyncResponseReader< ::DataResponse>* PrepareAsyncSaySignRaw(::grpc::ClientContext* context, const ::DataRequest& request, ::grpc::CompletionQueue* cq) override;
    const ::grpc::internal::RpcMethod rpcmethod_SaySign_;
  };
  static std::unique_ptr<Stub> NewStub(const std::shared_ptr< ::grpc::ChannelInterface>& channel, const ::grpc::StubOptions& options = ::grpc::StubOptions());

  class Service : public ::grpc::Service {
   public:
    Service();
    virtual ~Service();
    virtual ::grpc::Status SaySign(::grpc::ServerContext* context, const ::DataRequest* request, ::DataResponse* response);
  };
  template <class BaseClass>
  class WithAsyncMethod_SaySign : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    WithAsyncMethod_SaySign() {
      ::grpc::Service::MarkMethodAsync(0);
    }
    ~WithAsyncMethod_SaySign() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status SaySign(::grpc::ServerContext* /*context*/, const ::DataRequest* /*request*/, ::DataResponse* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    void RequestSaySign(::grpc::ServerContext* context, ::DataRequest* request, ::grpc::ServerAsyncResponseWriter< ::DataResponse>* response, ::grpc::CompletionQueue* new_call_cq, ::grpc::ServerCompletionQueue* notification_cq, void *tag) {
      ::grpc::Service::RequestAsyncUnary(0, context, request, response, new_call_cq, notification_cq, tag);
    }
  };
  typedef WithAsyncMethod_SaySign<Service > AsyncService;
  template <class BaseClass>
  class ExperimentalWithCallbackMethod_SaySign : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    ExperimentalWithCallbackMethod_SaySign() {
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
      ::grpc::Service::
    #else
      ::grpc::Service::experimental().
    #endif
        MarkMethodCallback(0,
          new ::grpc_impl::internal::CallbackUnaryHandler< ::DataRequest, ::DataResponse>(
            [this](
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
                   ::grpc::CallbackServerContext*
    #else
                   ::grpc::experimental::CallbackServerContext*
    #endif
                     context, const ::DataRequest* request, ::DataResponse* response) { return this->SaySign(context, request, response); }));}
    void SetMessageAllocatorFor_SaySign(
        ::grpc::experimental::MessageAllocator< ::DataRequest, ::DataResponse>* allocator) {
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
      ::grpc::internal::MethodHandler* const handler = ::grpc::Service::GetHandler(0);
    #else
      ::grpc::internal::MethodHandler* const handler = ::grpc::Service::experimental().GetHandler(0);
    #endif
      static_cast<::grpc_impl::internal::CallbackUnaryHandler< ::DataRequest, ::DataResponse>*>(handler)
              ->SetMessageAllocator(allocator);
    }
    ~ExperimentalWithCallbackMethod_SaySign() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status SaySign(::grpc::ServerContext* /*context*/, const ::DataRequest* /*request*/, ::DataResponse* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
    virtual ::grpc::ServerUnaryReactor* SaySign(
      ::grpc::CallbackServerContext* /*context*/, const ::DataRequest* /*request*/, ::DataResponse* /*response*/)
    #else
    virtual ::grpc::experimental::ServerUnaryReactor* SaySign(
      ::grpc::experimental::CallbackServerContext* /*context*/, const ::DataRequest* /*request*/, ::DataResponse* /*response*/)
    #endif
      { return nullptr; }
  };
  #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
  typedef ExperimentalWithCallbackMethod_SaySign<Service > CallbackService;
  #endif

  typedef ExperimentalWithCallbackMethod_SaySign<Service > ExperimentalCallbackService;
  template <class BaseClass>
  class WithGenericMethod_SaySign : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    WithGenericMethod_SaySign() {
      ::grpc::Service::MarkMethodGeneric(0);
    }
    ~WithGenericMethod_SaySign() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status SaySign(::grpc::ServerContext* /*context*/, const ::DataRequest* /*request*/, ::DataResponse* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
  };
  template <class BaseClass>
  class WithRawMethod_SaySign : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    WithRawMethod_SaySign() {
      ::grpc::Service::MarkMethodRaw(0);
    }
    ~WithRawMethod_SaySign() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status SaySign(::grpc::ServerContext* /*context*/, const ::DataRequest* /*request*/, ::DataResponse* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    void RequestSaySign(::grpc::ServerContext* context, ::grpc::ByteBuffer* request, ::grpc::ServerAsyncResponseWriter< ::grpc::ByteBuffer>* response, ::grpc::CompletionQueue* new_call_cq, ::grpc::ServerCompletionQueue* notification_cq, void *tag) {
      ::grpc::Service::RequestAsyncUnary(0, context, request, response, new_call_cq, notification_cq, tag);
    }
  };
  template <class BaseClass>
  class ExperimentalWithRawCallbackMethod_SaySign : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    ExperimentalWithRawCallbackMethod_SaySign() {
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
      ::grpc::Service::
    #else
      ::grpc::Service::experimental().
    #endif
        MarkMethodRawCallback(0,
          new ::grpc_impl::internal::CallbackUnaryHandler< ::grpc::ByteBuffer, ::grpc::ByteBuffer>(
            [this](
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
                   ::grpc::CallbackServerContext*
    #else
                   ::grpc::experimental::CallbackServerContext*
    #endif
                     context, const ::grpc::ByteBuffer* request, ::grpc::ByteBuffer* response) { return this->SaySign(context, request, response); }));
    }
    ~ExperimentalWithRawCallbackMethod_SaySign() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status SaySign(::grpc::ServerContext* /*context*/, const ::DataRequest* /*request*/, ::DataResponse* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
    virtual ::grpc::ServerUnaryReactor* SaySign(
      ::grpc::CallbackServerContext* /*context*/, const ::grpc::ByteBuffer* /*request*/, ::grpc::ByteBuffer* /*response*/)
    #else
    virtual ::grpc::experimental::ServerUnaryReactor* SaySign(
      ::grpc::experimental::CallbackServerContext* /*context*/, const ::grpc::ByteBuffer* /*request*/, ::grpc::ByteBuffer* /*response*/)
    #endif
      { return nullptr; }
  };
  template <class BaseClass>
  class WithStreamedUnaryMethod_SaySign : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    WithStreamedUnaryMethod_SaySign() {
      ::grpc::Service::MarkMethodStreamed(0,
        new ::grpc::internal::StreamedUnaryHandler< ::DataRequest, ::DataResponse>(std::bind(&WithStreamedUnaryMethod_SaySign<BaseClass>::StreamedSaySign, this, std::placeholders::_1, std::placeholders::_2)));
    }
    ~WithStreamedUnaryMethod_SaySign() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable regular version of this method
    ::grpc::Status SaySign(::grpc::ServerContext* /*context*/, const ::DataRequest* /*request*/, ::DataResponse* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    // replace default version of method with streamed unary
    virtual ::grpc::Status StreamedSaySign(::grpc::ServerContext* context, ::grpc::ServerUnaryStreamer< ::DataRequest,::DataResponse>* server_unary_streamer) = 0;
  };
  typedef WithStreamedUnaryMethod_SaySign<Service > StreamedUnaryService;
  typedef Service SplitStreamedService;
  typedef WithStreamedUnaryMethod_SaySign<Service > StreamedService;
};


#endif  // GRPC_DataOperation_2eproto__INCLUDED
