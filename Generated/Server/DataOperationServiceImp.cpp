#include <string>
#include <sstream>
#include "DataOperationServiceImp.h"

::grpc::Status DataOperationServiceImp::SaySign(::grpc::ServerContext* context, const::DataRequest* request, ::DataResponse* response)
{
	//iau data si o parsez
	std::string str = request->date();

	constexpr char DELIMITER = '/';

	//imi fac string pt zi ,luna ,an
	std::istringstream stm(str);
	std::string day, month, year;
	std::getline(stm, day, DELIMITER);
	std::getline(stm, month, DELIMITER);
	std::getline(stm, year);
	//le fac int-uri sa pot compara datele
	int d = std::stoi(day);
	int m = std::stoi(month);
	int y = std::stoi(year);

	//construiesc data request pt comparare
	int entryDate =(m * 100) + d;

	//iau vectorii cititi din fisier in main
	extern std::string start[12];
	extern std::string end[12];
	extern std::string result[12];

	//trec prin toate variantele posibile de zodii
	for (int i = 0; i < 12; i++) {
		//iau cand incepe si cand se termina o zodie
		std::istringstream stm1(start[i]);
		std::istringstream stm2(end[i]);
		//le parsez si pe ele
		std::string ds, ms;
		std::string de, me;
		std::getline(stm1, ds, DELIMITER);
		std::getline(stm1, ms, DELIMITER);
		std::getline(stm2, de, DELIMITER);
		std::getline(stm2, me, DELIMITER);

		//le fac int-uri sa le compar
		int Ds = std::stoi(ds);
		int Ms = std::stoi(ms);
		int De = std::stoi(de);
		int Me = std::stoi(me);

		//le adaug in comparatie
		int startDate =(Ms * 100) + Ds;
		int endDate = (Me * 100) + De;

		if (entryDate >= startDate && entryDate <= endDate) {
			//returnez zodia daca se incadreaza in acea data
			std::cout<<result[i]<<" \n";
			break;
		}
	}
	
	return ::grpc::Status::OK;
}
