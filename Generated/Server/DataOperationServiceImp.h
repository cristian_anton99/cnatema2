#pragma once
#include "DataOperation.grpc.pb.h"

class DataOperationServiceImp final : public DataOperationService::Service
{
	::grpc::Status SaySign(::grpc::ServerContext* context, const ::DataRequest* request, ::DataResponse* response) override;
};

