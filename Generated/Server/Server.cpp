#include <iostream>
#include <fstream>
#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>
#include "DataOperationServiceImp.h"
//vectori pt ce citesc din fisier
std::string start[12];//cand incepe
std::string end[12];//cand se termina
std::string result[12];//cum se numeste zodia

int main()
{
	std::ifstream f("file.txt");//declar fisier
	//citesc pe rand din fisier si le bag in vectori pt a le parsa in DataOperationServiceImp
	for (int i = 0; i < 12; i++) {
		f >> start[i] >> end[i]>>result[i];
	}

	std::string server_address("localhost:8888");
	DataOperationServiceImp service;

	::grpc_impl::ServerBuilder serverBuilder;

	serverBuilder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
	serverBuilder.RegisterService(&service);
	std::unique_ptr<::grpc_impl::Server> server(serverBuilder.BuildAndStart());
	std::cout << "Server listening on " << server_address << std::endl;
	server->Wait();
	f.close();
}