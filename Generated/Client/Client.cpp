#include <iostream>
#include <string>
#include <sstream>
#include <DataOperation.grpc.pb.h>
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

//functie verificare daca data contine doar cifre si '/'
bool Verify(std::string str) {
	for (auto i = 0; i < str.size(); i++) {
		if (!isdigit(str[i]) && str[i] != '/')return false;
	}
		return true;
}
//functie verifica daca este an bisect sau nu
	bool isLeap(int year)
	{
		return (((year % 4 == 0) &&
			(year % 100 != 0)) ||
			(year % 400 == 0));
	};
	//functie verificare data valida
bool DateCheck(std::string str) {
	const std::string date = str;
	constexpr char DELIMITER = '/';
	//parsez data
	std::istringstream stm(date);
	std::string day, month, year;
	std::getline(stm, day, DELIMITER);
	std::getline(stm, month, DELIMITER);
	std::getline(stm, year);
	int d = std::stoi(day);
	int m = std::stoi(month);
	int y = std::stoi(year);
	//iau niste ani pt o delimitare a anului
	const int MAX_VALID_YR = 9999;
	const int MIN_VALID_YR = 1800;
	//verificari generale
		if (y > MAX_VALID_YR ||
			y < MIN_VALID_YR)
			return false;
		if (m < 1 || m > 12)
			return false;
		if (d < 1 || d > 31)
			return false;
		//pt luna februarie
		if (m == 2)
		{
			if (isLeap(y))//este an bisect
				return (d <= 29);//are 29
			else
				return (d <= 28);//daca nu , 28
		}
		//verific daca are cumva 31 de zile, acestea fiind luni de 30 zile maxim
		if (m == 4 || m == 6 ||
			m == 9 || m == 11)
			return (d <= 30);

		return true;
};

int main()
{
	grpc_init();
	ClientContext context;
	auto sum_stub = DataOperationService::NewStub(grpc::CreateChannel("localhost:8888",
		grpc::InsecureChannelCredentials()));
	DataRequest dateRequest;
	std::cout << "Introduceti-va data nasterii: \n";
	std::string date = "";
	std::cin >> date;
	if (Verify(date) && DateCheck(date)) {//verificam daca este o data ok , ca sa o trimitem la server
		dateRequest.set_date(date);
		DataResponse dateResponse;
		auto Status = sum_stub->SaySign(&context, dateRequest, &dateResponse);
	}
	else
		std::cout << "Data introdusa este invalida.";//nu se trimite la server altfel
}

